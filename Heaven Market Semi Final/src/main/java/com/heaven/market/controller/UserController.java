package com.heaven.market.controller;

import com.heaven.market.controller.dto.LoginDTO;
import com.heaven.market.controller.dto.UserDTO;
import com.heaven.market.controller.stereotype.NoAuthentication;
import com.heaven.market.controller.stereotype.RequiredRoles;
import com.heaven.market.controller.enums.Role;
import com.heaven.market.service.ProfileService;
import com.heaven.market.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 27/04/2016.
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private ProfileService profileService;

    @NoAuthentication
    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public String createUser(@RequestBody UserDTO userDTO){
        String message = userService.checkUserDTO(userDTO); // validate
        if(message.equals("success")){
            userService.createUser(userDTO);
        }
        return message;
    }

    @RequiredRoles(Role.ADMIN)
    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    public List<UserDTO> getAllUsers(){
        return userService.getAllUsers();
    }

    @NoAuthentication
    @RequestMapping(value = "/{username}/profile/changePassword", method = RequestMethod.POST)
    public boolean changePassword(@RequestBody UserDTO userDTO) {
        return profileService.changePassword(userDTO);
    }

    @NoAuthentication
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public String doLogin(@RequestBody LoginDTO loginDTO){
        return userService.doLogin(loginDTO);
    }

    @RequiredRoles({Role.ADMIN, Role.REGISTER, Role.STUDENT})
    @RequestMapping(value = "/logout")
    public String logout(){
        return userService.logOut();
    }

    @NoAuthentication
    @RequestMapping(value = "/confirm/{username}/{role}", method = RequestMethod.GET)
    public String confirmAccount(@PathVariable("username") String username, @PathVariable("role") Role role){
        return userService.confirmRegister(username, role);
    }
}
