package com.heaven.market.service;

import com.heaven.market.controller.dto.LoginDTO;
import com.heaven.market.controller.dto.UserDTO;
import com.heaven.market.controller.enums.Role;
import com.heaven.market.model.User;
import com.heaven.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Created by Administrator on 27/04/2016.
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    HttpSession session = null;
    /*
    REGISTER
     */
    public User createUser(UserDTO userDTO){
        sendEmail(userDTO);

        User user = new User();
        user.setUsername(userDTO.getUsername());
        user.setPassword(tranformPassToMD5(userDTO.getPassword()));
        user.setEmail(userDTO.getEmail());
        if(userDTO.getRole().equals(Role.STUDENT))
            userDTO.setRole(Role.REGISTER);
        user.setRole(userDTO.getRole());
        user.setToken(createToken());
        user.setExpireDate(new Date(System.currentTimeMillis() + 1000*60*60*24));
        user.setTrustPoint(0);
        user.setStatusConfirm("Not Confirm");
        //Create Session
        session.setAttribute("username", userDTO.getUsername());

        return userRepository.save(user);
    }

    public List<UserDTO> getAllUsers(){
        List<User> users = (List<User>) userRepository.findAll();
        List<UserDTO> userDTOList = new ArrayList<UserDTO>();
        UserDTO userDTO = new UserDTO();
        for(User user:users ){
            userDTO.setUsername(user.getUsername());
            userDTO.setPassword(user.getPassword());
            userDTO.setRePassword(user.getPassword());
            userDTO.setRole(user.getRole());
            userDTO.setEmail(user.getEmail());
            userDTO.setTrustPoint(user.getTrustPoint());
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    public User createAdmin(){
        User user = new User();
        user.setExpireDate(new Date(System.currentTimeMillis() + 1000*60*60*24));
        user.setToken(createToken());
        user.setPassword("admin");
        user.setEmail("admin@gmail.com");
        user.setTrustPoint(100);
        user.setRole(Role.ADMIN);
        user.setUsername("Admin");
        userRepository.save(user);
        return user;
    }
    /*
    LOGIN
     */

    public String doLogin(LoginDTO loginDTO) {
        // 1. Generate token if not exist
        // 2. Set expired time for token
        User user = userRepository.findByUsername(loginDTO.getUsername());
        if(!user.getStatusConfirm().equals("Confirm"))
            return "Confirm by Your Email";
        // username is not correct
        if (user == null) return "Username is not existed";
        // password is not correct
        if (!user.getPassword().equals(tranformPassToMD5(loginDTO.getPassword())))
            return "Password is not correct";
        if (user.getToken() == null) {
            user.setToken(UUID.randomUUID().toString());
            user.setExpireDate(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24)));
        } else {
            user.setExpireDate(new Date(System.currentTimeMillis() + (1000 * 60 * 60 * 24)));
        }
        user = userRepository.save(user);
        return "success";
    }

    public String logOut(){
        session.removeAttribute("username");
        return "null";
    }

    public String confirmRegister(String username, Role role){
        User user = userRepository.findByUsername(username);
        user.setRole(role);
        user.setStatusConfirm("Confirm");
        return "success";
    }
    //FUNCTION EXTRA

    //Random UUID
    private String createToken(){
        return UUID.randomUUID().toString();
    }

    //Send and confirm by email
    private void sendEmail(UserDTO userDTO){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("hungpt58.uet@gmail.com","YOUR PASSWORD");
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("hungpt58.uet@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(userDTO.getEmail()));
            message.setSubject("Confirm Your Register");
            message.setText("Xin Chào bạn " + userDTO.getUsername());
            message.setContent("<p>Bạn click vào link để hoàn thành đăng kí : <h2><a href=http://localhost:8080/confirm/" + userDTO.getUsername() + "/" + userDTO.getRole() + ">Hoàn Thành Đăng Kí</a></h2></p>" , "text/html");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public String checkUserDTO(UserDTO userDTO) {
        User user = userRepository.findByUsername(userDTO.getUsername());
        if (user != null) {
            return "Username has already been used to create an account.";
        }
        else return "success";
    }

    public String tranformPassToMD5(String password){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
