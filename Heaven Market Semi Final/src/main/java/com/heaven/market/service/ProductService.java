package com.heaven.market.service;

import com.heaven.market.SearchEngine.InvertedIndex;
import com.heaven.market.SearchEngine.InvertedIndexRepository;
import com.heaven.market.controller.dto.ProductDTO;
import com.heaven.market.model.Category;
import com.heaven.market.model.Comment;
import com.heaven.market.model.Product;
import com.heaven.market.model.User;
import com.heaven.market.repository.CategoryRepository;
import com.heaven.market.repository.CommentRepository;
import com.heaven.market.repository.ProductRepository;
import com.heaven.market.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 27/04/2016.
 */
@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private InvertedIndexRepository invertedIndexRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CommentRepository commentRepository;

    public List<ProductDTO> addProduct(String nameStudent, ProductDTO productDTO){
        User user = userRepository.findByUsername(nameStudent);
        if (user == null) return null;

        if (!uploadImage(productDTO.getFiles(), nameStudent, unAccent(productDTO.getNameProductVN())).equals("success"))
            return null;
        //Đổi tên file theo mẫu
        String[] tmp = new String[3];
        String str = nameStudent + "_" + unAccent(productDTO.getNameProductVN());
        tmp[0] = str + "_01";
        tmp[1] = str + "_02";
        tmp[2] = str + "_03";

        Product product = new Product();
        product.setNameProductVN(productDTO.getNameProductVN());
        product.setNameProductENG(unAccent(fixSpacesString(productDTO.getNameProductVN())));
        product.setContentIntro(productDTO.getContentIntro());
        product.setPriceProduct(productDTO.getPriceProduct());
        product.setStatus(productDTO.getStatusProduct());
        product.setImage(tmp);
        List<Category> categoryList = new ArrayList<Category>();
        for (String categoryName : productDTO.getCategoryList()) {
            Category categoryObj = categoryRepository.findOne(categoryName);
            if (categoryObj == null) {
                categoryObj = new Category();
                categoryObj.setNameCategory(categoryName);
                categoryObj.setProductList(new ArrayList<Product>());
            }
            categoryObj.getProductList().add(product);
            categoryList.add(categoryObj);
            categoryObj = categoryRepository.save(categoryObj);
        }

        product.setCategoryList(categoryList);
        product = productRepository.save(product);

        user.getProductList().add(product);
        user = userRepository.save(user);
        createInvertedIndex(product);

        return showProducts(nameStudent);

    }

    public List<ProductDTO> showProducts(String username){
        List<Product> productList = userRepository.findByUsername(username).getProductList();
        List<ProductDTO> productDTOList = new ArrayList<ProductDTO>();
        for(Product product : productList){
            ProductDTO productDTO = new ProductDTO(product);
            productDTOList.add(productDTO);
        }
        return productDTOList;
    }

    public boolean deleteProduct(String username, int productID) {
        User user = userRepository.findByUsername(username);
        if (user == null ) return false;

        Product deleteProduct = productRepository.findOne(productID);
        productRepository.delete(productID);
        deleteInvertedIndex(deleteProduct);
        return user.getProductList().remove(deleteProduct);
    }

    public List<Comment> addComment(int productID, Comment comment) {
        Product product = productRepository.findOne(productID);
        if (product == null) return null;
        comment = commentRepository.save(comment);
        product.getCommentList().add(comment);
        product = productRepository.save(product);
        return product.getCommentList();
    }

    public String uploadImage( MultipartFile[] files, String username, String nameProduct){
        HttpServletRequest request = null;
        Model model = null;
        // Thư mục gốc upload file.
        String uploadRootPath = request.getServletContext().getRealPath("upload");
        System.out.println("uploadRootPath=" + uploadRootPath);

        File uploadRootDir = new File(uploadRootPath);
        //
        // Tạo thư mục gốc upload nếu nó không tồn tại.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        String str = username + "_" + nameProduct;
        //
        List<File> uploadedFiles = new ArrayList<File>();
        for (int i = 0; i < files.length; i++) {
            MultipartFile file = files[i];

            // Tên file gốc tại Client.
            String name = file.getOriginalFilename();

            if(isFileTypeImage(name) == false)
                return "File Không hợp lệ";
            String tmp = str + "_0" + Integer.toString(i + 1);

            if (name != null && name.length() > 0) {
                try {
                    byte[] bytes = file.getBytes();

                    // Tạo file tại Server.
                    File serverFile = new File(uploadRootDir.getAbsolutePath()
                            + File.separator + tmp);

                    // Luồng ghi dữ liệu vào file trên Server.
                    BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
                    stream.write(bytes);
                    stream.close();
                    //
                    uploadedFiles.add(serverFile);

                } catch (Exception e) {
                    return "error";
                }
            }
        }
        model.addAttribute("uploadedFiles", uploadedFiles);
        return "success";
    }
    /*
FUNCTION EXTRA
 */

    // remove mark of Vietnamese word.
    private String unAccent(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return  pattern.matcher(temp).replaceAll("").toLowerCase().replaceAll("đ", "d");
    }

    // Process space in string
    private static String fixSpacesString(String s) {
        return s.trim().replaceAll("\\s+", " ");
    }

    // Tao them du lieu cho InvertedIndex
    public void createInvertedIndex(Product product) {
        // Danh sach cac word trich xuat tu NameUnaccentProduct (ten sau khi bo dau)
        String[] wordList = product.getNameProductENG().split(" ");
        for (String word : wordList) {
            // key = word tuong ung trong inverted Index databse
            InvertedIndex key = invertedIndexRepository.findOne(word);
            if (key == null) { // key chua xuat hien trong database (new key)
                InvertedIndex newWord = new InvertedIndex(word, product.getProductID());
                invertedIndexRepository.save(newWord);
            } else {
                key.addSequence(product.getProductID());
            }
        }
    }
    // Xoa du lieu khi co 1 product bi xoa di
    public void deleteInvertedIndex(Product product) {
        String[] wordList = product.getNameProductENG().split(" ");
        for (String word : wordList) {
            InvertedIndex key = invertedIndexRepository.findOne(word);
            key.deleteProductID(product.getProductID());
        }
    }

    public List<Comment> deleteComment(int productID, int commentID) {
        Product product = productRepository.findOne(productID);
        Comment comment = commentRepository.findOne(commentID);
        product.getCommentList().remove(comment);
        product = productRepository.save(product);
        commentRepository.delete(commentID);

        return product.getCommentList();
    }

    //Kiểm tra image
    private boolean isFileTypeImage(String fileName) {
        String imagePattern =
                "([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)";
        return Pattern.compile(imagePattern).matcher(fileName).matches();
    }
}


