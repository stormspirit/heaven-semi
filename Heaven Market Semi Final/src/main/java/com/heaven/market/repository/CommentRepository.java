package com.heaven.market.repository;

/**
 * Created by son on 28/04/2016.
 */

import com.heaven.market.model.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer>{
}
