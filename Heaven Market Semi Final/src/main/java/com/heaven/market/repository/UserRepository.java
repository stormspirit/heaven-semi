package com.heaven.market.repository;

import com.heaven.market.model.User;
import com.heaven.market.repository.UserRepositoryCustom.UserRepositoryCustom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 27/04/2016.
 */
@Repository
public interface UserRepository extends CrudRepository<User, String>, UserRepositoryCustom {
    public User findByUsername(String username);
    public User findByToken(String token);
}
